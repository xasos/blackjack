package com.example.lab111.myapplication;


public class Card
{
    int cNum;
    String cSuit;
    String pathName;
    String cName;
    int cDrawable;

    public Card(String cName, int drawable)
    {
        pathName = "resources/" + cName + ".jpg";
        this.cName = cName;
        this.cDrawable = cDrawable;
        parseCardName();
    }

    public void parseCardName()
    {
        cSuit = cName.substring(0, 1);
        cNum = Integer.parseInt(cName.substring(1));
    }

    public int getCNum()
    {
        return cNum;
    }

    public String getCPath()
    {
        return pathName;
    }

    public String getCSuit()
    {
        return cSuit;
    }

    public int getCDrawable() { return cDrawable; }
}

