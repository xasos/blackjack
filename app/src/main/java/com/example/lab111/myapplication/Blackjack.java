package com.example.lab111.myapplication;

import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;


public class Blackjack extends ActionBarActivity {

    // Define global variables
    ImageView dealerCard1, dealerCard2;
    ImageView playerCard1, playerCard2, playerCard3, playerCard4, playerCard5, playerCard6, playerCard7, playerCard8, playerCard9, playerCard10, playerCard11;

    TextView currentBetAmountView, playerMoneyView;
    Button betButton;
    LinearLayout playerCards;
    private static final int STARTING_MONEY = 1000;
    private int playerMoney = 0;
    private int currentBet = 0;
    ArrayList<Card> cardDeck = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.black_jack);

        // add reference deck
        cardDeck.add(new Card("c1", R.drawable.c1));
        cardDeck.add(new Card("c2", R.drawable.c2));
        cardDeck.add(new Card("c3", R.drawable.c3));
        cardDeck.add(new Card("c4", R.drawable.c4));
        cardDeck.add(new Card("c5", R.drawable.c5));
        cardDeck.add(new Card("c6", R.drawable.c6));
        cardDeck.add(new Card("c7", R.drawable.c7));
        cardDeck.add(new Card("c8", R.drawable.c8));
        cardDeck.add(new Card("c9", R.drawable.c9));
        cardDeck.add(new Card("c10", R.drawable.c10));
        cardDeck.add(new Card("c11", R.drawable.c11));
        cardDeck.add(new Card("c12", R.drawable.c12));
        cardDeck.add(new Card("c13", R.drawable.c13));

        cardDeck.add(new Card("s1", R.drawable.s1));
        cardDeck.add(new Card("s2", R.drawable.s2));
        cardDeck.add(new Card("s3", R.drawable.s3));
        cardDeck.add(new Card("s4", R.drawable.s4));
        cardDeck.add(new Card("s5", R.drawable.s5));
        cardDeck.add(new Card("s6", R.drawable.s6));
        cardDeck.add(new Card("s7", R.drawable.s7));
        cardDeck.add(new Card("s8", R.drawable.s8));
        cardDeck.add(new Card("s9", R.drawable.s9));
        cardDeck.add(new Card("s10", R.drawable.s10));
        cardDeck.add(new Card("s11", R.drawable.s11));
        cardDeck.add(new Card("s12", R.drawable.s12));
        cardDeck.add(new Card("s13", R.drawable.s13));

        cardDeck.add(new Card("h1", R.drawable.h1));
        cardDeck.add(new Card("h2", R.drawable.h2));
        cardDeck.add(new Card("h3", R.drawable.h3));
        cardDeck.add(new Card("h4", R.drawable.h4));
        cardDeck.add(new Card("h5", R.drawable.h5));
        cardDeck.add(new Card("h6", R.drawable.h6));
        cardDeck.add(new Card("h7", R.drawable.h7));
        cardDeck.add(new Card("h8", R.drawable.h8));
        cardDeck.add(new Card("h9", R.drawable.h9));
        cardDeck.add(new Card("h10", R.drawable.h10));
        cardDeck.add(new Card("h11", R.drawable.h11));
        cardDeck.add(new Card("h12", R.drawable.h12));
        cardDeck.add(new Card("h13", R.drawable.h13));

        cardDeck.add(new Card("d1", R.drawable.d1));
        cardDeck.add(new Card("d2", R.drawable.d2));
        cardDeck.add(new Card("d3", R.drawable.d3));
        cardDeck.add(new Card("d4", R.drawable.d4));
        cardDeck.add(new Card("d5", R.drawable.d5));
        cardDeck.add(new Card("d6", R.drawable.d6));
        cardDeck.add(new Card("d7", R.drawable.d7));
        cardDeck.add(new Card("d8", R.drawable.d8));
        cardDeck.add(new Card("d9", R.drawable.d9));
        cardDeck.add(new Card("d10", R.drawable.d10));
        cardDeck.add(new Card("d11", R.drawable.d11));
        cardDeck.add(new Card("d12", R.drawable.d12));
        cardDeck.add(new Card("d13", R.drawable.d13));

        playerCards = (LinearLayout)findViewById(R.id.playerCards);

        playerMoney = STARTING_MONEY;

        //set images
        dealerCard1 = (ImageView) findViewById(R.id.dealerCard1);
        dealerCard1.setImageResource(R.drawable.back1);

        dealerCard2 = (ImageView) findViewById(R.id.dealerCard2);
        dealerCard2.setImageResource(R.drawable.back1);

        playerCard1 = (ImageView) findViewById(R.id.playerCard1);
        playerCard1.setImageResource(R.drawable.back2);

        playerCard2 = (ImageView) findViewById(R.id.playerCard2);
        playerCard2.setImageResource(R.drawable.back2);

        playerCard3 = (ImageView) findViewById(R.id.playerCard3);
        playerCard3.setImageResource(R.drawable.back2);

        playerCard4 = (ImageView) findViewById(R.id.playerCard4);
        playerCard4.setImageResource(R.drawable.back2);

        playerCard5 = (ImageView) findViewById(R.id.playerCard5);
        playerCard5.setImageResource(R.drawable.back2);

        playerCard6 = (ImageView) findViewById(R.id.playerCard6);
        playerCard6.setImageResource(R.drawable.back2);

        playerCard7 = (ImageView) findViewById(R.id.playerCard7);
        playerCard7.setImageResource(R.drawable.back2);

        playerCard8 = (ImageView) findViewById(R.id.playerCard8);
        playerCard8.setImageResource(R.drawable.back2);

        playerCard9 = (ImageView) findViewById(R.id.playerCard9);
        playerCard9.setImageResource(R.drawable.back2);

        playerCard10 = (ImageView) findViewById(R.id.playerCard10);
        playerCard10.setImageResource(R.drawable.back2);

        playerCard11 = (ImageView) findViewById(R.id.playerCard11);
        playerCard11.setImageResource(R.drawable.back2);

        currentBetAmountView = (TextView) findViewById(R.id.currentBetAmount);
        playerMoneyView = (TextView) findViewById(R.id.playerMoney);
        playerMoneyView.setText(Integer.toString(playerMoney));

        betButton = (Button) findViewById(R.id.betButton);
        betButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                playerMoney-=5;
                currentBet+=5;
                playerMoneyView.setText(Integer.toString(playerMoney));
                currentBetAmountView.setText(Integer.toString(currentBet));
            }
        });
    }

    public void changeCardView(ImageView cardLocation, String cardName) {
        cardLocation.setImageResource(R.drawable.c7);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
